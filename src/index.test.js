import expect  from 'expect';
import ecc     from './index';
import through from 'through';

// Emulate a bad stream
function badStream( loss ) {
  return through(function write(chunk) {
    if (Math.random() >= loss) this.queue(chunk);
  });
}

test('Ensuring data arrives (0% loss)', () => {
  expect(true).toBeTruthy();
});
